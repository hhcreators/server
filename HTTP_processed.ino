void HTTP_set_path(void) {
  HTTP.on("/config.json", handle_ConfigJSON);
  HTTP.on("/ssid", handle_Set_Ssid);
  HTTP.on("/ssidap", handle_Set_Ssidap);
  HTTP.on("/restart", handle_Restart);

  HTTP.on("/temp", handle_Temp);
  HTTP.begin();
}

void handle_Temp(){
    String t = HTTP.arg("t");
    String h = HTTP.arg("h");
    String nt = HTTP.arg("nt");
    socketSend(t + " " + h + " " + nt);
}

