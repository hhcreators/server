unsigned long startApTime;

bool WIFIinit() {
    byte tries = 20;
    WiFi.disconnect();
    WiFi.mode(WIFI_AP_STA);
    WiFi.begin(_ssid.c_str(), _password.c_str());
    while (--tries && WiFi.status() != WL_CONNECTED){
      Serial.print(".");
      delay(1000);
    }
    if(WiFi.status() != WL_CONNECTED) startAPMode();
    else {
    Serial.println("");
    Serial.println("Connected to station");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    return true;
    }
return false;
}

bool startAPMode(){
  startApTime = millis();
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(_ssidAP.c_str(), _passwordAP.c_str());
  Serial.println("WiFi access point created");
  Serial.print("IP address: ");
  Serial.println(apIP);
return true;
}

void tryToConnectWifi(bool attempt){
  if(attempt || (startApTime + 60000 <= millis() && !wifi_softap_get_station_num())){
      if(WIFIinit()) HTTP_set_path(); 
      else HTTP_init();
  }
}
