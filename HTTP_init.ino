void HTTP_init(void) {
  HTTP.on("/config.json", handle_ConfigJSON);
  HTTP.on("/ssid", handle_Set_Ssid);
  HTTP.on("/ssidap", handle_Set_Ssidap);
  HTTP.on("/restart", handle_Restart);
  HTTP.begin();
}

// http://192.168.4.1/ssid?ssid=home2&password=12345678
void handle_Set_Ssid() {
  _ssid = HTTP.arg("ssid");
  _password = HTTP.arg("password");
  saveConfig();
  HTTP.send(200, "text/plain", "OK");
}

//http://192.168.4.1/ssidap?ssidAP=home1&passwordAP=8765439 
void handle_Set_Ssidap() {
  _ssidAP = HTTP.arg("ssidAP");
  _passwordAP = HTTP.arg("passwordAP");
  saveConfig();
  HTTP.send(200, "text/plain", "OK"); 
}

//http://192.168.4.1/restart?device=ok
void handle_Restart() {
  String restart = HTTP.arg("device");
  if (restart == "ok") {
    HTTP.send(200, "text / plain", "Reset OK");
    ESP.restart();
  }
  else {
    HTTP.send(200, "text / plain", "No Reset");
  }
}

//{"ssid":"home","password":"12345678","ssidAP":"WiFi","passwordAP":"12345678","ip":"192.168.4.1"}
void handle_ConfigJSON() {
  String json = "{";
  json += "\",\"ssid\":\"";
  json += _ssid;
  json += "\",\"password\":\"";
  json += _password;
  json += "\",\"ssidAP\":\"";
  json += _ssidAP;
  json += "\",\"passwordAP\":\"";
  json += _passwordAP;
  json += "\",\"ip\":\"";
  json += WiFi.localIP().toString();
  json += "\"}";
  HTTP.send(200, "text/json", json);
}
