unsigned long lastConn;

void socketHandShake(){
  //Handshake with the server
  webSocketClient.path = socketPath;
  webSocketClient.host = socketHost;
  if (webSocketClient.handshake(client)) {
    Serial.println("Handshake successful");
  } else {
    Serial.println("Handshake failed.");
  }
}

void socketConnect(bool attempt){
//Connect to the websocket server
  if (attempt || lastConn + 5000 <= millis()){
    lastConn = millis();
    if (client.connect(socketHost, socketPort)) {
      Serial.println("Connected");
      socketHandShake();
    } else {
      Serial.println("Connection failed.");
    }
  }
}
void socketGet(){
  String data;
    if (client.connected()) {
      webSocketClient.getData(data);
      if (data.length() > 0) {
          Serial.print("SR: ");
          Serial.println(data);
          if(!data.equals("OK")) socketSend("OK");
          if(!configSended) {
            sendConfig(); 
          }
      }
  } else {
    Serial.println("Disconnected.");
    configSended = false;
    if(serverConnection()) socketConnect(0);
  }
}

void socketSend(String text){
  if (client.connected()) {
    text.trim();
    webSocketClient.sendData(text);
    Serial.print("SS: ");
    Serial.println(text);
} else {
    Serial.println("Disconnected.");
    configSended = false;
    if(serverConnection()) socketConnect(0);
  }
}
  
