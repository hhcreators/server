extern "C" {
  #include <user_interface.h>
  #include <espnow.h>
}
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <WebSocketClient.h>
#include <ESP8266httpUpdate.h>

IPAddress apIP(192, 168, 4, 1);
char socketPath[] = "/";
char socketHost[] = "192.168.0.103";
short socketPort = 13820;

WebSocketClient webSocketClient;
WiFiServer server(80);
WiFiClient client;
ESP8266WebServer HTTP(80);
File fsUploadFile;

String _ssid     = "";
String _password = "";
String _ssidAP = "";
String _passwordAP = "";
String jsonConfig = "{}";
bool configSended = false;

void setup() {
  Serial.begin(230400);
  Serial.println("");
  FS_init();
  Serial.println("File system started !");
  loadConfig();
  Serial.println("Config file loaded !");
  Serial.print("WIFI initalizing...");
  openConnection();
  espNowInit();
  Serial.println("ESP-Now ready !");
}

void loop() {
  HTTP.handleClient();
  delay(1);
 if(serverConnection()) {
    socketGet();
    delay(10);
  }
}



