void webUpdateFS(String link) {
  // включаем перезагрузку после обновления FS
  ESPhttpUpdate.rebootOnUpdate(true);
  //Обнавляем FS
  t_httpUpdate_return ret = ESPhttpUpdate.updateSpiffs(link);
  // включаем перезагрузку после прошивки
}

void webUpdateCode(String link) {
  // включаем перезагрузку после прошивки
  ESPhttpUpdate.rebootOnUpdate(true);
  // Перепрошиваем модуль
  t_httpUpdate_return ret = ESPhttpUpdate.update(link);
}
