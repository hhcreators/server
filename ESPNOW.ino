#define NUMSLAVES 20
struct Slave{
  uint8_t mac[6];
};
Slave slaves[NUMSLAVES];

void printMacAddress(uint8_t* macaddr) {
  Serial.print("{");
  for (int i = 0; i < 6; i++) {
    Serial.print("0x");
    Serial.print(macaddr[i], HEX);
    if (i < 5) Serial.print(',');
  }
  Serial.println("}");
}

void espNowInit() {
  if (esp_now_init()==0) {
    esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
    
    registerSlaves();
    
    esp_now_register_recv_cb([](uint8_t *macaddr, uint8_t *data, uint8_t len) {
    Serial.println("recv_cb");

    Serial.print("mac address: ");
    printMacAddress(macaddr);

    Serial.print("data: ");

    socketSend((char*)data);
    Serial.println((char*)data);
    Serial.println("");
    uint8_t macaddr1[6];
  wifi_get_macaddr(STATION_IF, macaddr1);
  Serial.print("mac address (STATION_IF): ");
  printMacAddress(macaddr1);
  });
  
  esp_now_register_send_cb([](uint8_t* macaddr, uint8_t status) {
    Serial.println("send_cb");

    Serial.print("mac address: ");
    printMacAddress(macaddr);

    Serial.print("status = "); Serial.println(status);
  });
  
    Serial.println("direct link  init ok");
  } else {
    Serial.println("dl init failed");
    ESP.restart();
    return;
  }
}

bool registerSlaves(){
  uint8_t mac[] = {0x2E,0x3A,0xE8,0xB,0x2D,0xD7};
  esp_now_add_peer(mac, (uint8_t)ESP_NOW_ROLE_SLAVE, 1, NULL, 0);
  return true;
}
